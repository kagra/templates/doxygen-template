# Version used during development
cmake_minimum_required(VERSION 3.20)

# Project information
set(PROJECT_TITLE "My Library C++ Template")
project(MyLib 
    VERSION 0.1
    DESCRIPTION "Lorem ipsum dolor sit amet"
    HOMEPAGE_URL "https://git.ligo.org/kagra/templates/doxygen-template"
)

set(LIBRARY ${PROJECT_NAME})
set(LIBRARY_TITLE ${PROJECT_TITLE})

# Additional cmake features
add_compile_options(-Wextra -Wno-sign-compare -Wimplicit-fallthrough)
set(CMAKE_INSTALL_MESSAGE "LAZY")

# Check if git submodule are initialized in case you use custom cmake modules.
file(GLOB CMAKE_MODULE_PATHS "${CMAKE_CURRENT_SOURCE_DIR}/cmake/*")
foreach(MODULE_PATH ${CMAKE_MODULE_PATHS})
    if(IS_DIRECTORY ${MODULE_PATH})
        list(PREPEND CMAKE_MODULE_PATH ${MODULE_PATH})
    endif()
endforeach()

#
# Prepare project architecture
include(ProjectArchitecture)
include(FindPackageStandard)
include(BuildOptions)
include(DisplayMotd)

DISPLAY_MOTD()
MESSAGE_TITLE("${PROJECT_TITLE}")

update_build_options()
check_build_option(BUILD_IMAGE "Docker container won't be built.")
check_build_option(BUILD_DOCUMENTATION "Library documentation won't be built.")
check_build_option(BUILD_TESTS "Tests for this library won't be running.")
check_build_option(BUILD_LIBRARY "Library `${PROJECT_NAME}` will not be built.")
check_build_option(BUILD_EXAMPLES "Examples how-to-use `${PROJECT_NAME}` won't be computed.")
check_build_option(BUILD_TOOLS "Additional binary tools won't be computed.")

#
# Build project
if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake/.env.in AND NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/.env)
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/.env.in ${CMAKE_CURRENT_SOURCE_DIR}/.env @ONLY)
endif()
if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake/thislib.sh.in)
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/thislib.sh.in   thislib.${LIBRARY}.sh   @ONLY )
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/thislib.${LIBRARY}.sh DESTINATION ${CMAKE_INSTALL_PREFIX} 
            PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE OPTIONAL)
endif()
if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake/thislib.csh.in)
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/thislib.csh.in  thislib.${LIBRARY}.csh  @ONLY )
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/thislib.${LIBRARY}.csh DESTINATION ${CMAKE_INSTALL_PREFIX} 
            PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE OPTIONAL)
endif()
if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/build AND NOT CMAKE_CURRENT_BINARY_DIR STREQUAL ${CMAKE_CURRENT_SOURCE_DIR}/build)
    FILE(CREATE_LINK ${CMAKE_BINARY_DIR_RELATIVE} ${CMAKE_CURRENT_SOURCE_DIR}/build SYMBOLIC)
endif()

if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/include/lib${LIBRARY}.Config.h.in)
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/include/lib${LIBRARY}.Config.h.in ${CMAKE_CURRENT_BINARY_DIR}/include/lib${LIBRARY}.Config.h @ONLY)
endif()

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include DESTINATION ${CMAKE_INSTALL_PREFIX} 
PATTERN "*.in" EXCLUDE PATTERN ".*" EXCLUDE)
install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/include DESTINATION ${CMAKE_INSTALL_PREFIX} 
PATTERN "*.in" EXCLUDE PATTERN ".*" EXCLUDE)

#
# Create docker image (used in GitLab CI/CD for running tests)
if(BUILD_IMAGE AND EXISTS Dockerfile)
    include(DockerContainer)
endif()

#
# Additional restriction/warning feature
include(DisableInSourceBuild)
include(GitBranchWarning)

# Project overview
DUMP_PROJECT()

#
# Create a library
if(BUILD_LIBRARY)

    #
    # Load compiler
    include(StandardCompilerC)
    include(StandardCompilerCXX)

    find_package(Date REQUIRED)
    #
    # Add library
    FILE_SOURCES(SOURCES "src" ABSOLUTE)

    add_library(${PROJECT_NAME} SHARED ${SOURCES})
    target_include_directories(${PROJECT_NAME} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
    target_include_directories(${PROJECT_NAME} PUBLIC "${CMAKE_CURRENT_BINARY_DIR}/include")

    target_link_package(${PROJECT_NAME} PRIVATE Date)

    # Installation of the library
    install(TARGETS ${PROJECT_NAME} LIBRARY DESTINATION lib ARCHIVE DESTINATION lib RUNTIME DESTINATION bin)
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include DESTINATION . PATTERN "*.in" EXCLUDE PATTERN ".*" EXCLUDE)

    if(${CMAKE_MAINPROJECT})
        add_custom_target(lib DEPENDS ${LIBRARY} COMMENT "Compiling library")
    endif()

    # 
    # Add tests
    if(BUILD_TESTS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tests)
        add_subdirectory("tests")
    endif()

    # 
    # Compilation of examples
    if(BUILD_EXAMPLES AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/examples)
        add_subdirectory("examples")
    endif()

    # 
    # Compilation of tools
    if(BUILD_TOOLS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tools)
        add_subdirectory("tools")
    endif()

    dump_target_dependencies()

endif()

#
# Prepare assets for documentation
if(BUILD_DOCUMENTATION AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/share/doxygen)
    add_subdirectory("share/doxygen")
endif()

#
# Hook Scripts
if(CMAKE_MAINPROJECT)

    # Make sure cmake includes are available
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/cmake 
            DESTINATION share 
            PATTERN "*.in" EXCLUDE 
            PATTERN ".*" EXCLUDE
            PATTERN "README*" EXCLUDE
    )

    # Post install message
    find_file(CMAKE_POST_CONFIG PostInstallConfig.cmake PATHS ${CMAKE_MODULE_PATHS})
    if(CMAKE_POST_CONFIG)
        install(SCRIPT "${CMAKE_POST_CONFIG}")
    endif()

    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        check_target_dependencies()
    endif()

    # Post cmake message
    include(PostCMakeConfig)
endif()
