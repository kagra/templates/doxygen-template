#pragma once

#include "libMyLib.Config.h"
#include "mylib/Bar.h"

namespace MyLib {

    /**
     * @class Foo
     * @brief Represents a class derived from Bar within the MyLib namespace.
     */
    class Foo : public Bar {
    public:
    
        /**
         * @brief Enumerated type representing dictionary elements A, B, C.
         */
        enum Dict { A, B, C };

        Foo();
        ~Foo();

        std::string SayHi(const std::string& name) override;
        // Add any necessary methods or members for Foo
    };
}