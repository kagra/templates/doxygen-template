#pragma once

#include <iostream>
#include <vector>

template <typename Element>
class MyClass {

    private:
        class Impl;
        Impl *pImpl;

    protected:
        std::vector<Element> indices;

    public:

        MyClass();
        MyClass(std::vector<Element> indices);
        virtual ~MyClass();

        template <typename... Indices>
        MyClass(Indices... indices): MyClass(std::vector<Element>({indices...})) {}

        void Print();
};
