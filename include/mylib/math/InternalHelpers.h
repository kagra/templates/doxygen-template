#pragma once

#include <iostream>
#include <cmath> // Changed from <math.h> to <cmath> for C++ standard library

namespace MyLib {

    /**
     * @file InternalHelpers.h
     * @brief Contains helper functions and constants within the MyLib namespace.
     */

    /**
     * @brief A constant integer representing a dummy value.
     */
    const static int dummy = 718460100;

    /**
     * @brief Adds two integers.
     * @param A The first integer.
     * @param B The second integer.
     * @return The result of the addition.
     */
    inline static int Add(int A, int B) { return A + B; }

    /**
     * @brief Subtracts one integer from another.
     * @param A The first integer (minuend).
     * @param B The second integer (subtrahend).
     * @return The result of the subtraction.
     */
    inline static int Sub(int A, int B) { return A - B; }

    /**
     * @brief Multiplies two integers.
     * @param A The first integer.
     * @param B The second integer.
     * @return The result of the multiplication.
     */
    inline static int Mul(int A, int B) { return A * B; }

    /**
     * @brief Divides one integer by another.
     * @param A The dividend.
     * @param B The divisor.
     * @return The result of the division as a double. Returns NaN if the divisor is 0.
     */
    inline static double Div(int A, int B) { return B == 0 ? NAN : A / static_cast<double>(B); }
};
