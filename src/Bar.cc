#include "mylib/Bar.h"

namespace MyLib {

    /**
     * @brief Constructor for the Bar class.
     * Prints a message when an object of Bar is created.
     */
    Bar::Bar() {
        std::cout << "Hi I'm Bar !" << std::endl;
    }

    /**
     * @brief Destructor for the Bar class.
     * Prints a farewell message when an object of Bar is destroyed.
     */
    Bar::~Bar() {
        std::cout << "Goodbye! I am Bar and I'm leaving now." << std::endl;
    }

    /**
     * @brief Greets a specified name.
     * @param name The name to greet.
     * @return A greeting message including the name.
     */
    std::string Bar::SayHi(const std::string& name) {
        return std::string("Hi " + name + ", I'm Bar!");
    }
}