#include "mylib/MyClass.h"
#include "mylib/impl/MyClassImpl.h"

template<typename Element>
MyClass<Element>::MyClass() { pImpl = new MyClass<Element>::Impl(); }

template<typename Element>
MyClass<Element>::MyClass(std::vector<Element> indices) : MyClass() 
{
    this->indices = indices;
}

template<typename Element>
void MyClass<Element>::Print()
{
    pImpl->Print(indices);
}

template<typename Element>
MyClass<Element>::~MyClass() {
    if (pImpl) {
        delete pImpl;
        pImpl = nullptr;
    }
}

template class MyClass<int>;
template class MyClass<float>;
template class MyClass<double>;