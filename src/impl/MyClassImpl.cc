#include "mylib/impl/MyClassImpl.h"

template <typename Element>
void MyClass<Element>::Impl::Print(const std::vector<Element>& indices) {
    std::cout << "MyClass(" << indices.size() << " indices)" << std::endl;

    for (auto idx : indices) {
        std::cout << "Index: " << idx << std::endl;
    }

    auto now = std::chrono::system_clock::now();
    
    // Print the current time in a default format
    std::cout << "Current time: " 
              << date::format("%F %T", now) << std::endl; // e.g., 2025-02-03 15:21:09
}

template class MyClass<int>::Impl;
template class MyClass<float>::Impl;
template class MyClass<double>::Impl;