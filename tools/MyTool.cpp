/**
 * @file main.cpp
 * @brief A simple C++ program printing "Hello World!" to the console.
 */

#include <iostream>
#include "mylib/MyClass.h"

/**
 * @brief The main function, the entry point of the program.
 * @return 0 on successful execution.
 */
int main() {
    // Prints "Hello World!" to the console
    std::cout << "Hello World !" << std::endl;
    MyClass(std::vector<int>({1,2,3})).Print();

    return 0; // Returning 0 to indicate successful execution
}